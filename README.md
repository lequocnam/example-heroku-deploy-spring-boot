####  Deploy to heroku with Bitbucket Pipes

This repo contains a basic example for deploying applications to heroku using [heroku-deploy](https://bitbucket.org/atlassian/heroku-deploy/src/master/) pipe.

#### Hot wo use thie example

##### Forking the repo
Create a fork of this repo, see [Forking a Repository](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html).

##### Enabling Pipelines in your repo 

To enable Piplines go to *Project*  > *Settings* > *Pipelines* and toggle the **Enable Pipelines** button. This can also be done from the **Pipelines** tab in your repository sidebar when you enable Pipelines for the first time. See also [Piplines Getting started](https://confluence.atlassian.com/bitbucket/get-started-with-bitbucket-pipelines-792298921.html) for more instructions.

##### Configuring pipe variables

You also need to configure Pipelines variables to use this example. To do this you need a Heroku API key and an app name. If you don't already yave a Heroku account. If you don't, go to [https://heroku.com]() and create one. The next step is to set up an application [https://dashboard.heroku.com/new-app](https://dashboard.heroku.com/new-app). Use the App name you entered as `HEROKU_APP_NAME` Pipeline variable. Then navigate to the Heroku [Accout Settings](https://dashboard.heroku.com/account) page and get or create an API key and use it as a `HEROKU_API_KEY`Pipeline variable. See **User-defined variables** section in [Variables in pipelines](https://confluence.atlassian.com/bitbucket/variables-in-pipelines-794502608.html) for how to configure Pipelines variables. 

#### Verifying the results of the pipe run

After you've enabled Pipelines and configured all variables you can navigate to the Pipelines section of your fork and explore the current status of the builds. If the build is :successful: you should be able to navigate to `https://<your-app-name>.herokuapp.com/` URL to see the sample python app being served. In addition to that you can go to `https://dashboard.heroku.com/apps/<your-app-name>` to inspect the application in details.

